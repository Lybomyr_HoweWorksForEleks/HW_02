﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Department : IComparable
    {
        List<Book> booksInDepartment = new List<Book>();

        public string Name { get; set; }

        public List<Book> BooksInDepartment
        {
            get
            {
                return booksInDepartment;
            }

            set
            {
                booksInDepartment = value;
            }
        }

        public Department(string name)
        {
            this.Name = name;
        }

        int IComparable.CompareTo(object obj)
        {
            Department item = obj as Department;
            if (item != null)
            {
                return this.booksInDepartment.Count.CompareTo(item.booksInDepartment.Count);
            }
            else
            {
                throw new ArgumentException("Argument is not Department!");
            }
        }

        public override string ToString()
        {
            return "Department name - " + this.Name +
                " |Number of books in department - " + this.booksInDepartment.Count;

        }
    }
}
