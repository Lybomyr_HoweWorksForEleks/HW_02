﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
    static class UsingLinqToObject
    {
        public static List<Book> SortingBooks(Library library)
        {
            List<Book> sortedBooks;

            var BooksQuery = from department in library.listOfDepartments
                        from book in department.BooksInDepartment
                        orderby book.BookName
                        select book;
            sortedBooks = BooksQuery.ToList<Book>();
            
            return sortedBooks;
        }

        public static Book SearchBook(Library library, string BookName, out string departmentName)
        {
            Book resultBook = null;
            departmentName = "";
            var BooksQuery = from department in library.listOfDepartments
                             from book in department.BooksInDepartment
                             where book.BookName == BookName
                             let Name = department.Name
                             select new
                             {
                                 book = book,
                                 departmentName = Name
                             };

            foreach (var item in BooksQuery)
            {
                resultBook = item.book;
                departmentName = item.departmentName;
            }
            
            return resultBook;
        }
    }
}
