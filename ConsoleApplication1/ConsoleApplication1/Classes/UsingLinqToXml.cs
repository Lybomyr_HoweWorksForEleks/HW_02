﻿using System;
using System.Xml.Linq;

namespace ConsoleApplication1
{
    static class UsingLinqToXml
    {
        public static Library library;

        public static void PushDataToXmlFile()
        {
            if (library != null)
            {
                XDocument xDoc = new XDocument();

                XElement XMLlibrary = new XElement("Library");
                XAttribute LibraryName = new XAttribute("LibraryName", library.NameOfLibrary);
                XMLlibrary.Add(LibraryName);
                foreach (var department in library.listOfDepartments)
                {
                    XElement newDepartment = new XElement("Department");
                    XAttribute departmentName = new XAttribute("DepartmentName", department.Name);
                    XAttribute departmentBookCount = new XAttribute("NumberOfBooks", department.BooksInDepartment.Count);
                    newDepartment.Add(departmentName);
                    newDepartment.Add(departmentBookCount);
                    foreach (var book in department.BooksInDepartment)
                    {
                        XElement newBook = new XElement("Book");
                        XAttribute bookName = new XAttribute("BookName", book.BookName);
                        XAttribute authorName = new XAttribute("AuthorName", book.AuthorName);
                        XAttribute numbOfPages = new XAttribute("NumbOfPages", book.NumbOfPages);
                        newBook.Add(bookName);
                        newBook.Add(authorName);
                        newBook.Add(numbOfPages);

                        newDepartment.Add(newBook);
                    }

                    XMLlibrary.Add(newDepartment);
                }
                xDoc.Add(XMLlibrary);
                xDoc.Save("xmlFile.xml");
            }

            else
            {
                Console.WriteLine("Please enter data for library!");
            }
            
        }

        public static Library ReadXmlFile(string path)
        {
            XDocument xDoc = XDocument.Load(path);
            XElement libraryElement = xDoc.Element("Library");
            XAttribute libraryName = libraryElement.Attribute("LibraryName");
            Library newLibrary = new Library(libraryName.Value);
            Department newdepartment;
            Book newBook;
           
            foreach (XElement department in xDoc.Element("Library").Elements("Department"))
            {
                XAttribute name = department.Attribute("DepartmentName");
                XAttribute NumberOfBooks = department.Attribute("NumberOfBooks");

                newdepartment = new Department(name.Value);
                
                foreach (XElement book in department.Elements("Book"))
                {
                    XAttribute bookName = book.Attribute("BookName");
                    XAttribute authorName = book.Attribute("AuthorName");
                    XAttribute numbOfPages = book.Attribute("NumbOfPages");

                    newdepartment.BooksInDepartment.Add(newBook = new Book(bookName.Value, Convert.ToInt32(numbOfPages.Value), authorName.Value));
                }
                newLibrary.listOfDepartments.Add(newdepartment);

            }
            return newLibrary;
        }
        
    }
}
